@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            @if (session()->exists('adopteunelicorne_session') == true)
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <a href="{{ route('eleveurs.index') }}" class="btn btn-success" title="Gérer les éleveurs">Gérer les éleveurs</a>
                    
                    <a href="{{ route('reproducteurs.index') }}" class="btn btn-success" title="Gérer les reproducteurs">Gérer les reproducteurs</a>
                </div>
            </div>
            @endif
            @if (session()->exists('adopteunelicorne_session') == false)
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    
                    <a href="{{ route('licorns.index') }}" class="btn btn-success" title="Liste des licornes">Liste des licornes</a>
                </div>
            </div>
            @endif
        </div>
    </div>
</div>
@endsection
