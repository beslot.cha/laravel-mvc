@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Modification d'un elevage</div>

                <div class="card-body">

                    <a href="{{ route('reproducteurs.index') }}" class="btn btn-danger" title="Retour à l'index">Retour à l'index</a>
                    <form action="{{ route('elevages.update', $elevage->id) }}" method="POST">
                        @csrf
                        @method('PUT')
                        <label for="name">Nom</label>
                        @if(!is_null($elevage->name))
                            <input id="name" type="text" name="name" value="{{ $elevage->name }}">

                            <br>

                            <label for="description">Description</label>
                            <input id="description" type="text" name="description" value="{{ $elevage->description }}">

                            <br>
                        @else
                            <input id="name" type="text" name="name">

                            <br>

                            <label for="description">Description</label>
                            <input id="description" type="text" name="description">

                            <br>
                        @endif
                        <br>
                        <button type="submit">Envoyer</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
