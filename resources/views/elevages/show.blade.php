@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Affichage de l'elevage : {{ $elevage->name }}</div>

                <div class="card-body">

                    <a class="btn btn-success" href="{{ route('reproducteurs.index') }}">Retour à l'index</a>

                    <a class="btn btn-warning" href="{{ route('elevages.edit', $elevage->id) }}">Modifier</a>

                    <form action="{{ route('elevages.destroy') }}" method="POST">
                        @csrf
                        @method('DELETE')
                        <input type="hidden" name="id" value="{{ $elevage->id }}">
                        <button class="btn btn-danger" type="submit">Supprimer</button>
                    </form>
                    <br>
                    <br>
                    
                    Nom :
                    <br>
                    {{$elevage->name}}
                    <br>
                    <br>
                    Description :
                    <br>
                    {{$elevage->description}}

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
