@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Création d'un reproducteur</div>

                <div class="card-body">

                    <a href="{{ route('reproducteurs.index') }}" class="btn btn-danger" title="Retour a la home">Page précédente</a>

                    <form action="{{ route('reproducteurs.store') }}" method="POST">
                        @csrf
                        <label for="name">Nom</label>
                        <input id="name" type="text" name="name">
                        <br>
                        <button type="submit">Envoyer</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
