@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Reproducteurs</div>

                <div class="card-body">

                    <a href="{{ route('home') }}" class="btn btn-secondary" title="Retour sur la page d'accueil">Retour sur la page d'accueil</a>

                    <br>

                    <a href="{{ route('reproducteurs.create') }}" class="btn btn-success" title="Ajouter un reproducteur">Ajouter un reproducteur</a>
                    
                    <br>

                    <ul>
                        @foreach($reproducteurs as $reproducteur)
                            <li>
                                <a href="{{ route('reproducteurs.show', $reproducteur->id) }}" title="{{ $reproducteur->name }}">{{ $reproducteur->name }}</a>
                            </li>
                        @endforeach
                    </ul>
                   
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
