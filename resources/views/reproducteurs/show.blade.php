@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Affichage de la catégorie : {{ $reproducteur->name }}</div>

                <div class="card-body">

                    <a class="btn btn-success" href="{{ route('reproducteurs.index') }}">Retour à l'index</a>

                    <br>

                    <a class="btn btn-warning" href="{{ route('reproducteurs.edit', $reproducteur->id) }}">Modifier</a>

                    <br>

                    <a class="btn btn-success" href="{{ route('elevages.create', $reproducteur->id) }}">Ajouter un elevage</a>

                    <br>
                    
                    <form action="{{ route('reproducteurs.destroy') }}" method="POST">
                    @csrf
                    @method('DELETE')
                    <input type="hidden" name="id" value="{{ $reproducteur->id }}">
                    <button class="btn btn-danger" type="submit">Supprimer</button>
                    </form>
                   
                    @if(!is_null($reproducteur->elevages))
                    <h3>liste des elevagees liés à cet éleveur</h3>

                    <ul>

                        @foreach($reproducteur->elevages as $elevage)
                            <li>
                            <a href="{{ route('elevages.show', $elevage->id) }}">{{ $elevage->name }}</a>
                            </li>
                        @endforeach

                    </ul>
                    @endif

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
