@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Licornes</div>

                <div class="card-body">

                    <a href="{{ route('home') }}" class="btn btn-secondary" title="Retour sur la page d'accueil">Retour sur la page d'accueil</a>

                    <br>

                    <ul>
                        @foreach($licorns as $licorn)
                            <li>
                                <a href="{{ route('licorns.show', $licorn->id) }}" title="{{ $licorn->name }}">{{ $licorn->name }}</a>
                            </li>
                        @endforeach
                    </ul>
                   
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
