@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Création d'une licorne</div>

                <div class="card-body">

                    <a href="{{ route('eleveurs.index') }}" class="btn btn-danger" title="Retour à l'index">Retour à l'index</a>

                    <form action="{{ route('licorns.store') }}" method="POST">
                        @csrf
                        <input id="name" type="hidden" name="eleveur_id" value="{{ $id }}">

                        <label for="name">Nom</label>
                        <input id="name" type="text" name="name">

                        <br>

                        <label for="description">Description</label>
                        <input id="description" type="text" name="description">

                        <br>

                        <label for="price">Prix</label>
                        <input id="price" type="text" name="price">

                        <br>
                        
                        <button type="submit">Envoyer</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
