@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Modification d'une licorne</div>

                <div class="card-body">

                    <a href="{{ route('eleveurs.index') }}" class="btn btn-danger" title="Retour à l'index">Retour à l'index</a>
                    
                    <form action="{{ route('licorns.update', $licorn->id) }}" method="POST">
                        @csrf
                        @method('PUT')
                        <label for="name">Nom</label>
                        @if(!is_null($licorn->name))
                            <input id="name" type="text" name="name" value="{{ $licorn->name }}">

                            <br>

                            <label for="description">Description</label>
                            <input id="description" type="text" name="description" value="{{ $licorn->description }}">

                            <br>

                            <label for="price">Prix</label>
                            <input id="price" type="text" name="price" value="{{ $licorn->price }}">

                            <br>
                        @else
                            <input id="name" type="text" name="name">

                            <br>

                            <label for="description">Description</label>
                            <input id="description" type="text" name="description">

                            <br>

                            <label for="price">Prix</label>
                            <input id="price" type="text" name="price">

                            <br>
                        @endif
                        <br>
                        <button type="submit">Envoyer</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
