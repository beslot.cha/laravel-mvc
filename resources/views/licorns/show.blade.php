@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Affichage de la licorne : {{ $licorn->name }}</div>

                <div class="card-body">

                @if (session()->exists('adopteunelicorne_session') == true)
                    <a class="btn btn-success" href="{{ route('eleveurs.index') }}">Retour à l'index</a>
                    
                    <a class="btn btn-warning" href="{{ route('licorns.edit', $licorn->id) }}">Modifier</a>

                    <form action="{{ route('licorns.destroy') }}" method="POST">
                        @csrf
                        @method('DELETE')
                        <input type="hidden" name="id" value="{{ $licorn->id }}">
                        <button class="btn btn-danger" type="submit">Supprimer</button>
                    </form>
                    <br>
                    <br>
                @endif
                @if (session()->exists('adopteunelicorne_session') == false)
                
                    <a href="{{ route('licorns.index') }}" class="btn btn-secondary" title="Retour sur la liste">Retour sur la liste</a>
                    <br>
                    <a href="{{ route('licorns.index') }}" class="btn btn-danger" title="Acheter la licorne">Acheter la licorne</a>
                    <br>
                    <br>
                @endif

                    
                    Nom :
                    <br>
                    {{$licorn->name}}
                    <br>
                    <br>
                    Description :
                    <br>
                    {{$licorn->description}}
                    <br>
                    <br>
                    Prix :
                    <br>
                    {{$licorn->price}}

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
