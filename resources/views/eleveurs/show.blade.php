@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Affichage de la catégorie : {{ $eleveur->name }}</div>

                <div class="card-body">

                    <a class="btn btn-success" href="{{ route('eleveurs.index') }}">Retour à l'index</a>

                    <br>

                    <a class="btn btn-warning" href="{{ route('eleveurs.edit', $eleveur->id) }}">Modifier</a>

                    <br>

                    <a class="btn btn-success" href="{{ route('licorns.create', $eleveur->id) }}">Ajouter une licorne</a>

                    <br>
                    
                    <form action="{{ route('eleveurs.destroy') }}" method="POST">
                    @csrf
                    @method('DELETE')
                    <input type="hidden" name="id" value="{{ $eleveur->id }}">
                    <button class="btn btn-danger" type="submit">Supprimer</button>
                    </form>
                   
                    @if(!is_null($eleveur->licorns))
                    <h3>liste des licornes liés à cet éleveur</h3>

                    <ul>

                        @foreach($eleveur->licorns as $licorn)
                            <li>
                            <a href="{{ route('licorns.show', $licorn->id) }}">{{ $licorn->name }}</a>
                            </li>
                        @endforeach

                    </ul>
                    @endif

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
