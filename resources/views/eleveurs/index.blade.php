@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Eleveurs</div>

                <div class="card-body">

                    <a href="{{ route('home') }}" class="btn btn-secondary" title="Retour sur la page d'accueil">Retour sur la page d'accueil</a>

                    <br>

                    <a href="{{ route('eleveurs.create') }}" class="btn btn-success" title="Ajouter un éleveur">Ajouter un éleveur</a>
                    
                    <br>

                    <ul>
                        @foreach($eleveurs as $eleveur)
                            <li>
                                <a href="{{ route('eleveurs.show', $eleveur->id) }}" title="{{ $eleveur->name }}">{{ $eleveur->name }}</a>
                            </li>
                        @endforeach
                    </ul>
                   
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
