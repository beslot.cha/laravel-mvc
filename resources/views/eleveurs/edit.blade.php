@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Modification d'un éleveur</div>

                <div class="card-body">

                    <a href="{{ route('eleveurs.index') }}" class="btn btn-danger" title="Retour à l'index">Retour à l'index</a>
                    <form action="{{ route('eleveurs.update', $eleveur->id) }}" method="POST">
                        @csrf
                        @method('PUT')
                        <label for="name">Nom</label>
                        @if(!is_null($eleveur->name))
                            <input id="name" type="text" name="name" value="{{ $eleveur->name }}">
                        @else
                            <input id="name" type="text" name="name">
                        @endif
                        <br>
                        <button type="submit">Envoyer</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
