<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Eleveur;
use App\Models\Licorn;

class EleveurController extends Controller
{
    public function create()
    {
        return view('eleveurs.create');
    }

    public function store(Request $request)
    {
        $eleveur = new Eleveur();
        $eleveur->name = $request->get('name');
        $eleveur->save();
        
        return redirect()->route('eleveurs.index');
    }

    public function show($id)
    {
        $eleveur = Eleveur::find($id);
        
        return view('eleveurs.show', compact('eleveur'));
    }

    public function index()
    {
        $eleveurs = Eleveur::all();

        return view('eleveurs.index', compact('eleveurs'));
    }

    public function edit($id)
    {
        $eleveur = Eleveur::find($id);

        return view('eleveurs.edit', compact('eleveur'));
    }

    public function update(Request $request, $id)
    {
        $eleveur = Eleveur::find($id);
        $eleveur->name = $request->get('name');
        $eleveur->save();
        
        return redirect()->route('eleveurs.index');
    }

    public function destroy(Request $request)
    {
        $eleveur = Eleveur::find($request->get('id'));
        $eleveur->delete();

        return redirect()->route('eleveurs.index');
    }
}
