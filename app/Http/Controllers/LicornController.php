<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Licorn;
use App\Models\Eleveur;

class LicornController extends Controller
{
    public function create($id)
    {
        $licorns = Licorn::all();
        return view('licorns.create', compact('licorns', 'id'));
    }

    public function store(Request $request)
    {
        $licorn = new Licorn();
        $licorn->name = $request->get('name');
        $licorn->description = $request->get('description');
        $licorn->price = $request->get('price');
        $licorn->eleveur_id = $request->get('eleveur_id');
        $licorn->save();
        
        return redirect()->route('eleveurs.show', $request->get('eleveur_id'));
    }

    public function show($id)
    {
        $licorn = Licorn::with('eleveur')->find($id);

        return view('licorns.show', compact('licorn'));
    }

    public function index()
    {
        $licorns = Licorn::all();

        return view('licorns.index', compact('licorns'));
    }

    public function edit($id)
    {
        $licorn = Licorn::find($id);

        return view('licorns.edit', compact('licorn'));
    }

    public function update(Request $request, $id)
    {
        $licorn = Licorn::find($id);
        $licorn->name = $request->get('name');
        $licorn->description = $request->get('description');
        $licorn->price = $request->get('price');
        $licorn->save();
        
        return redirect()->route('eleveurs.index');
    }

    public function destroy(Request $request)
    {
        $licorn = Licorn::find($request->get('id'));
        $licorn->delete();

        return redirect()->route('eleveurs.index');
    }
}
