<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Elevage;
use App\Models\Reproducteur;

class ElevageController extends Controller
{
    public function create($id)
    {
        $elevages = Elevage::all();
        return view('elevages.create', compact('elevages', 'id'));
    }

    public function store(Request $request)
    {
        $elevage = new Elevage();
        $elevage->name = $request->get('name');
        $elevage->description = $request->get('description');
        $elevage->reproducteur_id = $request->get('reproducteur_id');
        $elevage->save();
        
        return redirect()->route('reproducteurs.show', $request->get('reproducteur_id'));
    }

    public function show($id)
    {
        $elevage = Elevage::with('reproducteur')->find($id);

        return view('elevages.show', compact('elevage'));
    }

    public function index()
    {
        $elevages = Elevage::all();

        return view('elevages.index', compact('elevages'));
    }

    public function edit($id)
    {
        $elevage = Elevage::find($id);

        return view('elevages.edit', compact('elevage'));
    }

    public function update(Request $request, $id)
    {
        $elevage = Elevage::find($id);
        $elevage->name = $request->get('name');
        $elevage->description = $request->get('description');
        $elevage->save();
        
        return redirect()->route('reproducteurs.index');
    }

    public function destroy(Request $request)
    {
        $elevage = Elevage::find($request->get('id'));
        $elevage->delete();

        return redirect()->route('reproducteurs.index');
    }
}
