<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Reproducteur;
use App\Models\Elevage;

class ReproducteurController extends Controller
{
    public function create()
    {
        return view('reproducteurs.create');
    }

    public function store(Request $request)
    {
        $reproducteur = new Reproducteur();
        $reproducteur->name = $request->get('name');
        $reproducteur->save();
        
        return redirect()->route('reproducteurs.index');
    }

    public function show($id)
    {
        $reproducteur = Reproducteur::find($id);
        
        return view('reproducteurs.show', compact('reproducteur'));
    }

    public function index()
    {
        $reproducteurs = Reproducteur::all();

        return view('reproducteurs.index', compact('reproducteurs'));
    }

    public function edit($id)
    {
        $reproducteur = Reproducteur::find($id);

        return view('reproducteurs.edit', compact('reproducteur'));
    }

    public function update(Request $request, $id)
    {
        $reproducteur = Reproducteur::find($id);
        $reproducteur->name = $request->get('name');
        $reproducteur->save();
        
        return redirect()->route('reproducteurs.index');
    }

    public function destroy(Request $request)
    {
        $reproducteur = Reproducteur::find($request->get('id'));
        $reproducteur->delete();

        return redirect()->route('reproducteurs.index');
    }
}
