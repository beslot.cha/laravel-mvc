<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Licorn extends Model
{
    protected $table = "licorns";

    protected $fillable = [
        "id", "name", "descritpion", "price", "eleveur_id"
    ];

    public function eleveur()
    {
        return $this->belongsTo(Eleveur::class, 'eleveur_id');
    }
}
