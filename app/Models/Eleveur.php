<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Eleveur extends Model
{
    protected $table = "eleveurs";

    protected $fillable = [
        "id", "name"
    ];

    public function licorns()
    {
        return $this->hasMany(Licorn::class, 'eleveur_id');
    }
}
