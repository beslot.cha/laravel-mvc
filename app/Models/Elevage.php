<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Elevage extends Model
{
    protected $table = "elevages";

    protected $fillable = [
        "id", "name", "descritpion", "reproducteur_id"
    ];

    public function reproducteur()
    {
        return $this->belongsTo(Reproducteur::class, 'reproducteur_id');
    }
}
