<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Reproducteur extends Model
{
    protected $table = "reproducteurs";

    protected $fillable = [
        "id", "name"
    ];

    public function elevages()
    {
        return $this->hasMany(Elevage::class, 'reproducteur_id');
    }
}