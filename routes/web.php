<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/eleveurs', 'EleveurController@index')->name('eleveurs.index');
Route::get('/eleveurs/{id}/show', 'EleveurController@show')->name('eleveurs.show');
Route::get('/eleveurs/create', 'EleveurController@create')->name('eleveurs.create');
Route::get('/eleveurs/{id}/edit', 'EleveurController@edit')->name('eleveurs.edit');
Route::post('/eleveurs', 'EleveurController@store')->name('eleveurs.store');
Route::put('/eleveurs/{id}', 'EleveurController@update')->name('eleveurs.update');
Route::delete('/eleveurs', 'EleveurController@destroy')->name('eleveurs.destroy');

Route::get('/reproducteurs', 'ReproducteurController@index')->name('reproducteurs.index');
Route::get('/reproducteurs/{id}/show', 'ReproducteurController@show')->name('reproducteurs.show');
Route::get('/reproducteurs/create', 'ReproducteurController@create')->name('reproducteurs.create');
Route::get('/reproducteurs/{id}/edit', 'ReproducteurController@edit')->name('reproducteurs.edit');
Route::post('/reproducteurs', 'ReproducteurController@store')->name('reproducteurs.store');
Route::put('/reproducteurs/{id}', 'ReproducteurController@update')->name('reproducteurs.update');
Route::delete('/reproducteurs', 'ReproducteurController@destroy')->name('reproducteurs.destroy');

Route::get('/licorns', 'LicornController@index')->name('licorns.index');
Route::get('/licorns/{id}/show', 'LicornController@show')->name('licorns.show');
Route::get('/licorns/{id}/create', 'LicornController@create')->name('licorns.create');
Route::get('/licorns/{id}/edit', 'LicornController@edit')->name('licorns.edit');
Route::post('/licorns', 'LicornController@store')->name('licorns.store');
Route::put('/licorns/{id}', 'LicornController@update')->name('licorns.update');
Route::delete('/licorns', 'LicornController@destroy')->name('licorns.destroy');

Route::get('/elevages', 'ElevageController@index')->name('elevages.index');
Route::get('/elevages/{id}/show', 'ElevageController@show')->name('elevages.show');
Route::get('/elevages/{id}/create', 'ElevageController@create')->name('elevages.create');
Route::get('/elevages/{id}/edit', 'ElevageController@edit')->name('elevages.edit');
Route::post('/elevages', 'ElevageController@store')->name('elevages.store');
Route::put('/elevages/{id}', 'ElevageController@update')->name('elevages.update');
Route::delete('/elevages', 'ElevageController@destroy')->name('elevages.destroy');
